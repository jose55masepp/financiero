package co.com.developer.test.controller;

import co.com.developer.entity.CuentaEntity;
import co.com.developer.test.BaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CuentaController extends BaseTest {


    ObjectMapper om = new ObjectMapper();

    @Before
    public void inicio() {
        super.init();
    }

    @Test
    void listCuenta() throws Exception{
        CuentaEntity cuentaEntity = new CuentaEntity();
        Mockito.when(cuentaRepository.findAll())
                .thenReturn(Arrays.asList(cuentaEntity));
        mockMvc.perform(get(urlRuta.concat("clientes"))
                .header("Origin","*"))
                .andExpect(status().isOk());
    }
}
