package co.com.developer.test.controller;

import co.com.developer.entity.ClienteEntity;
import co.com.developer.test.BaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClienteControllerTest extends BaseTest {

    ObjectMapper om = new ObjectMapper();

    @Before
    public void inicio() {
        super.init();
    }

    @Test void listClient() throws Exception{
        ClienteEntity clienteEntity = new ClienteEntity();
        Mockito.when(clienteRepository.findAll())
                .thenReturn(Arrays.asList(clienteEntity));
        mockMvc.perform(get(urlRuta.concat("clientes"))
                .header("Origin","*"))
                .andExpect(status().isOk());
    }
}
