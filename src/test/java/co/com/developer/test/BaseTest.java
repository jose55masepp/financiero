package co.com.developer.test;

import java.text.SimpleDateFormat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import co.com.developer.repository.CuentaRepository;
import co.com.developer.repository.ClienteRepository;
import co.com.developer.repository.MovimientoRepository;

import javax.validation.constraints.Max;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public abstract class BaseTest {

    /**
     * The model mapper.
     */
    protected ModelMapper modelMapper;

    /**
     * The object mapper.
     */
    protected ObjectMapper objectMapper;

    protected String urlRuta = "/v1.0/";

    protected static final SimpleDateFormat FECHA = new SimpleDateFormat("yyyy-MM-dd");
    protected static final SimpleDateFormat FECHA_HORA = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @MockBean
    protected CuentaRepository cuentaRepository;

    @MockBean
    protected MovimientoRepository movimientoRepository;

    @MockBean
    protected ClienteRepository clienteRepository;


    @Autowired
    protected MockMvc mockMvc;

    protected String TEST = "test";

    protected void init() {
        this.modelMapper = new ModelMapper();
        this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    }
}
