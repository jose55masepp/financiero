package co.com.developer.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jcastaneda
 */
@Entity
@Table(name = "CUENTA")
public class CuentaEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Size(max = 20)
    @Column(name = "MONEDA")
    private String moneda;
    @Column(name = "MOVIMIENTO")
    private BigInteger movimiento;
    @Column(name = "CLIENTE")
    private BigInteger cliente;

    public CuentaEntity() {
    }

    public CuentaEntity(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigInteger getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(BigInteger movimiento) {
        this.movimiento = movimiento;
    }

    public BigInteger getCliente() {
        return cliente;
    }

    public void setCliente(BigInteger cliente) {
        this.cliente = cliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuentaEntity)) {
            return false;
        }
        CuentaEntity other = (CuentaEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cmg.entity.CuentaEntity[ id=" + id + " ]";
    }
    
}
