/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.developer.serviceImpl;

import co.com.developer.entity.ClienteEntity;
import co.com.developer.service.ClienteService;
import co.com.developer.repository.ClienteRepository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jcastaneda
 */
@Service
public class ClienteServiceImpl implements ClienteService {

    
    ClienteRepository clienteRepository;
    
    @Autowired
    public ClienteServiceImpl(ClienteRepository clienteRepository){
        super();
        this.clienteRepository = clienteRepository;
    }
    
    @Override
    public ClienteEntity save(ClienteEntity cliente) {
        return this.clienteRepository.save(cliente);
    }

    @Override
    public List<ClienteEntity> list() {
        return this.clienteRepository.findAll();
    }

    @Override
    public void delete(BigDecimal id) {
        this.clienteRepository.deleteById(id);
    }

    @Override
    public ClienteEntity findById(BigDecimal id) {
        return this.clienteRepository.getOne(id);
    }
    
}
