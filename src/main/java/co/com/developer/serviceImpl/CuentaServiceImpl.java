package co.com.developer.serviceImpl;

import co.com.developer.entity.CuentaEntity;
import co.com.developer.repository.CuentaRepository;
import co.com.developer.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

public class CuentaServiceImpl implements CuentaService {

    CuentaRepository cuentaRepository;

    @Autowired
    public CuentaServiceImpl(CuentaRepository cuentaRepository){
        super();
        this.cuentaRepository = cuentaRepository;
    }

    @Override
    public CuentaEntity save(CuentaEntity cuenta) {
        return this.cuentaRepository.save(cuenta);
    }

    @Override
    public List<CuentaEntity> list() {
        return  this.cuentaRepository.findAll();
    }

    @Override
    public void delete(BigDecimal id) {
        this.cuentaRepository.deleteById(id);
    }

    @Override
    public CuentaEntity findById(BigDecimal id) {
        return  this.cuentaRepository.getOne(id);
    }
}
