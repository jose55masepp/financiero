/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.developer.test.controller;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 *
 * @author jcastaneda
 */
public abstract class BaseController implements Serializable{
    /** The creado correctamente. */
    protected static String EJECUTADO_CORRECTAMENTE = "Solicitud ejecutada con éxito";

    protected static final int CODIGO_200 = HttpStatus.OK.value();
}
