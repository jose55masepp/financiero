package co.com.developer.test.controller;

import co.com.developer.dto.Respuesta;
import co.com.developer.entity.CuentaEntity;
import co.com.developer.service.CuentaService;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author jcastaneda
 */

@RequestMapping("/v1.0/cuenta")
@RestController
public class CuentaController extends BaseController{

    private static final long serialVersionUID = -3926172889564903286L;
    private CuentaService cuentaService;

    @Autowired
    public CuentaController(CuentaService cuentaService){
        this.cuentaService = cuentaService;
    }

    @GetMapping()
    private Respuesta<List<CuentaEntity>> listCuenta(){
        return new Respuesta<List<CuentaEntity>>(EJECUTADO_CORRECTAMENTE, CODIGO_200,
                this.cuentaService.list());
    }

    @PostMapping()
    private Respuesta<CuentaEntity> guardarCuenta(
            @RequestBody Map<String, String> json
    ) throws Exception {
        CuentaEntity cuenta = new CuentaEntity();
        return new Respuesta<CuentaEntity>(EJECUTADO_CORRECTAMENTE, CODIGO_200,
                cuentaService.save(cuenta));
    }
}
