/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.developer.test.controller;

import co.com.developer.dto.Respuesta;
import co.com.developer.entity.ClienteEntity;
import co.com.developer.service.ClienteService;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author jcastaneda
 */

@RequestMapping("/v1.0/clientes")
@RestController
public class ClienteController extends BaseController{
    private static final long serialVersionUID = -3926172889564903286L;
    private ClienteService clienteService;
    
    @Autowired
    public ClienteController(ClienteService clienteService){
        this.clienteService = clienteService;
    }
    
    @GetMapping()
    private Respuesta<List<ClienteEntity>> listClient(){
        return new Respuesta<List<ClienteEntity>>(EJECUTADO_CORRECTAMENTE, CODIGO_200,
                this.clienteService.list());
    }

    @PostMapping()
    private Respuesta<ClienteEntity> guardarCliente(
            @RequestBody Map<String, String> json
    ) throws Exception {
        ClienteEntity cliente = new ClienteEntity();
        return new Respuesta<ClienteEntity>(EJECUTADO_CORRECTAMENTE, CODIGO_200,
                clienteService.save(cliente));
    }
    
}
