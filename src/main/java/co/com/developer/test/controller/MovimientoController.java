package co.com.developer.test.controller;

import co.com.developer.dto.Respuesta;
import co.com.developer.entity.MovimientoEntity;
import co.com.developer.service.MovimientoService;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author jcastaneda
 */

@RequestMapping("/v1.0/movimietos")
@RestController
public class MovimientoController extends BaseController{
    private static final long serialVersionUID = -3926172889564903286L;
    private MovimientoService movimientoService;

    @Autowired
    public MovimientoController(MovimientoService movimientoService){
        this.movimientoService = movimientoService;
    }

    @GetMapping()
    private Respuesta<List<MovimientoEntity>> listMovimiento(){
        return new Respuesta<List<MovimientoEntity>>(EJECUTADO_CORRECTAMENTE, CODIGO_200,
                this.movimientoService.list());
    }

    @PostMapping()
    private Respuesta<MovimientoEntity> guardarMovimiento(
            @RequestBody Map<String, String> json
    ) throws Exception {
        MovimientoEntity movimientoEntity = new MovimientoEntity();
        return new Respuesta<MovimientoEntity>(EJECUTADO_CORRECTAMENTE, CODIGO_200,
                movimientoService.save(movimientoEntity));
    }
}
