/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.developer.service;

import co.com.developer.entity.CuentaEntity;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author jcastaneda
 */
@Service
public interface CuentaService {
    
    CuentaEntity save(CuentaEntity movimiento);
    List<CuentaEntity> list();
    void delete(BigDecimal id);
    CuentaEntity findById(BigDecimal id);
}
