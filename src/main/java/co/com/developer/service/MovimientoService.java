/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.developer.service;

import co.com.developer.entity.MovimientoEntity;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author jcastaneda
 */
@Service
public interface MovimientoService {
    
    MovimientoEntity save(MovimientoEntity movimiento);
    List<MovimientoEntity> list();
    void delete(BigDecimal id);
    MovimientoEntity findById(BigDecimal id);
}
