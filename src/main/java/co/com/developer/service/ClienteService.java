/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.developer.service;

import co.com.developer.entity.ClienteEntity;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author jcastaneda
 */
public interface ClienteService {
    
    ClienteEntity save(ClienteEntity movimiento);
    List<ClienteEntity> list();
    void delete(BigDecimal id);
    ClienteEntity findById(BigDecimal id);
}
