package co.com.developer.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * The Class Respuesta.
 *
 * @param <T> the generic type
 */

public class Respuesta<T> implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6029373099166095579L;

	/** The Constant DATE_FORMAT. */
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	/** The code. */
	protected int code;
	
	/** The date time. */
	protected String dateTime;
	
	/** The message. */
	protected String message;
	
	/** The data. */
	protected T data;
	
	/**
	 * Instantiates a new respuesta.
	 *
	 * @param message the message
	 * @param code the code
	 * @param data the data
	 */
	
	/**
	 * Instantiates a new respuesta.
	 *
	 * @param message the message
	 * @param isSuccess the is success
	 * @param code the code
	 * @param data the data
	 */
	public Respuesta(String message,int code,T data) {
		this(code,new SimpleDateFormat(DATE_FORMAT).format(Calendar.getInstance().getTime()),message,data);
	}
	
	/**
	 * Instantiates a new respuesta.
	 *
	 * @param code the code
	 * @param dateTime the date time
	 * @param message the message
	 * @param data the data
	 */
	public Respuesta(int code, String dateTime, String message, T data) {
		this.dateTime = dateTime;
		this.message = message;
		this.data = data;
		this.code = code;
	}

	public Respuesta(String message, int code) {
		this(code,new SimpleDateFormat(DATE_FORMAT).format(Calendar.getInstance().getTime()),message,null);// TODO Auto-generated constructor stub
	}

	public Respuesta() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * Gets the date time.
	 *
	 * @return the date time
	 */
	public String getDateTime() {
		return dateTime;
	}

	/**
	 * Sets the date time.
	 *
	 * @param dateTime the new date time
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(T data) {
		this.data = data;
	}
	
}
