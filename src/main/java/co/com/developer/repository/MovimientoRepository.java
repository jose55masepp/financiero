package co.com.developer.repository;

import co.com.developer.entity.MovimientoEntity;

import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jcastaneda
 */
@Repository
public interface MovimientoRepository extends JpaRepository<MovimientoEntity, BigDecimal>{
    
}
