/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.developer.repository;

import co.com.developer.entity.CuentaEntity;

import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author jcastaneda
 */
@Repository
public interface CuentaRepository extends JpaRepository<CuentaEntity, BigDecimal>{
    
}
